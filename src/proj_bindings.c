/*
MIT License

Copyright (c) 2017 likeno

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//#include <projects.h>
#define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H
#include <proj_api.h>
#include <string.h>

/*
 * error string
 */
int bind_pj_strerrno(int err, char *err_msg)
{
    int result;
    char* msg = pj_strerrno(err);
    if (msg != NULL)
        strcpy(err_msg, msg);
    result = pj_ctx_get_errno(pj_get_default_ctx());
    // Clear all errors from the default context
    pj_ctx_set_errno(pj_get_default_ctx(), 0);
    return result;
}

/*
 * initialise projection structure
 */
int bind_pj_init_plus(projPJ *prjdefn, char *args)
{
    int result;
    *prjdefn = pj_init_plus(args);
    result = pj_ctx_get_errno(pj_get_default_ctx());
    // Clear all errors from the default context
    pj_ctx_set_errno(pj_get_default_ctx(), 0);
    return result;
}

/*
 * free projection structure
 */
int bind_pj_free(projPJ *prjdefn)
{
    int result;
    pj_free(*prjdefn);
    result = pj_ctx_get_errno(pj_get_default_ctx());
    // Clear all errors from the default context
    pj_ctx_set_errno(pj_get_default_ctx(), 0);
    return result;
}

/*
 * transform projection
 */
int bind_pj_transform(projPJ *srcdefn, projPJ *dstdefn,
                         long point_count, int point_offset,
                         double *x, double *y, double *z)
{
    int result;
    pj_transform(*srcdefn, *dstdefn,
                 point_count, point_offset, x, y, z);

    result = pj_ctx_get_errno(pj_get_default_ctx());
    // Clear all errors from the default context
    pj_ctx_set_errno(pj_get_default_ctx(), 0);
    return result;
}
