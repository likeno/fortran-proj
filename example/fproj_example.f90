! MIT License
! 
! Copyright (c) 2018 likeno
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in all
! copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
! SOFTWARE.


!--------------------------------------------------------------------------!
! This example shows how to use Fortran-Proj to compute the coordinates    !
! transformation from the 'geos' projection (Geostationary Satellite View) !
! into long/lat (Geodetic WGS84) and Albers Equal Area coordinates.        !
! The source reference system coordinates will be set according with the   !
! MetoSAT-MSG images, which are available in the Geostationary projection. !
!--------------------------------------------------------------------------!

program fproj_example

  use fproj
  implicit none

  !-------------------------------------------------------------------------!
  ! MeteoSAT-MSG image parameters: (Geostationary projection)               !
  !-------------------------------------------------------------------------!
  integer, parameter      :: image_size_x = 3712, image_size_y = 3712
  real(kind=8), parameter :: image_resolution_geos = DEG2RAD * 2._8**(16) /&
                                                     13642337 * 35785863._8
  real(kind=8), parameter :: upperleft_x_geos = 1.0 * (-1857) *&
                                                image_resolution_geos
  real(kind=8), parameter :: upperleft_y_geos = 1.0 * 1857 *&
                                                image_resolution_geos
  !-------------------------------------------------------------------------!

  !------------------------------------------!
  ! PROJ variables:                          !
  !------------------------------------------!
  type(fproj_prj)     :: src_proj, dst_proj
  character(len=256)  :: proj_str
  integer             :: fproj_status
  !------------------------------------------!

  !-----------------------------------------------------------------!
  ! Auxiliary variables:                                            !
  !-----------------------------------------------------------------!
  real(kind=8)        :: x1D(image_size_x), y1D(image_size_y),&
                         x2D(image_size_y, image_size_x),&
                         y2D(image_size_y, image_size_x),&
                         long(image_size_y, image_size_x),&
                         lat(image_size_y, image_size_x),&
                         lon_point, lat_point
                         
  integer             :: iter_pix, iter_x, iter_y
  !-----------------------------------------------------------------!


print*,&
NEW_LINE('A')//&
"Fortran-Proj example:"//&
NEW_LINE('A')//NEW_LINE('A')//&
"This example shows how to use Fortran-Proj to compute the coordinates    "//&
NEW_LINE('A')//&
"transformation from the 'geos' projection (Geostationary Satellite View) "//&
NEW_LINE('A')//&
"into longlat (Geodetic WGS84) and Albers Equal Area coordinates.         "//&
NEW_LINE('A')//&
"The source reference system coordinates will be set according with the   "//&
NEW_LINE('A')//&
"MetoSAT-MSG images, which are available in the Geostationary projection. "//&
NEW_LINE('A')



  !------------------------------------------------------!
  ! Create the x, y coordinates for each line and column !
  ! of the image, in the source coordinates reference    !
  ! system (Geostationary projection):                   !
  !------------------------------------------------------!
  x1D = [(iter_pix, iter_pix=1, image_size_x)]*&
        image_resolution_geos + upperleft_x_geos

  y1D = [(iter_pix, iter_pix=1, image_size_y)]*&
        (-image_resolution_geos) + upperleft_y_geos

  !--------------------------------------------------------!
  ! Spread the x, y coordinates into a 2D grid in order to !
  ! get a pixel by pixel representation of the respective  !
  ! coordinates:                                           !
  !--------------------------------------------------------!
  x2D = spread(x1D, 1, image_size_y)
  y2D = spread(y1D, 2, image_size_x)

  !------------------------------------------------!
  ! Print a 10x10 pixels sample of the image       !
  ! coordinates (Geostationary projection):        !
  !------------------------------------------------!
  print*, "Image coordinates in the Geostationary projection:"
  print*, "(10x10 pixels sample; image lines:[591, 600]; image columns:[1601, 1610])"//NEW_LINE('A')
  print '("x (geos):"/(10F12.2))', ((x2D(iter_y, iter_x), iter_x=1601, 1610), iter_y=591, 600)
  print '("y (geos):"/(10F12.2))', ((y2D(iter_y, iter_x), iter_x=1601, 1610), iter_y=591, 600)
  print*, NEW_LINE('A')

  !-------------------------------------------------------!
  ! Now we are going into PROJ usage!                     !
  !                                                       !
  ! Define the Geostationary projection to be used as the !
  ! source coordinates reference system:                  !
  !-------------------------------------------------------!
  proj_str = '+proj=geos '//&
             '+lon_0=0 '//&
             '+h=35785863.0 '//&
             '+x_0=0 '//&
             '+y_0=0 '//&
             '+a=6378137.0 '//&
             '+b=6356752.3'//&
             '+units=m '//&
             '+sweep=y '//&
             '+no_defs' 

  fproj_status = fproj_init(src_proj, proj_str)
  if (fproj_status .ne. FPROJ_NOERR) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if

  !---------------------------------------------------------------!
  ! Use the fproj_inv function to transform the image coordinates !
  ! from the source projection (Geostationary) into latlong       !
  ! (Geodetic WGS84) coordinates:                                 !    
  !---------------------------------------------------------------!

  !------------------------------------------------------------!
  ! Lets test a single point transformation for the upper-left !
  ! and lower-right pixels of the displayed sample:            !
  !------------------------------------------------------------!
  print*,"Upper-left coorner of the displayed sample:"
  fproj_status = fproj_inv(src_proj, x2D(591, 1601), y2D(591, 1601), lon_point, lat_point)
  if (fproj_status .ne. FPROJ_NOERR) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if  
  print*,"long: ", lon_point
  print*,"lat: ", lat_point
  print*, NEW_LINE('A')

  print*,"Lower-right coorner of the displayed sample:"
  fproj_status = fproj_inv(src_proj, x2D(600, 1610), y2D(600, 1610), lon_point, lat_point)
  if (fproj_status .ne. FPROJ_NOERR) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if  
  print*,"long: ", lon_point
  print*,"lat: ", lat_point
  print*, NEW_LINE('A')

  !------------------------------------------------!
  ! Now for the full image coordinates (2D array). !
  ! Here we also take the opportunity to test the  !
  ! FPROJ_TOLERANCE_CONDITION_ERR                  !
  !------------------------------------------------!
  fproj_status = fproj_inv(src_proj, x2D, y2D, long, lat)
  print*, fproj_strerrno(fproj_status)
  print*, NEW_LINE('A')
  print*, "You will stop your program at this moment if you don't allow any errors..."//&
          NEW_LINE('A')//NEW_LINE('A')//&
          "Sometimes there are areas of the image which doesn't represent any part of the"//&
          NEW_LINE('A')//&
          "globe. In these cases, the FPROJ_TOLERANCE_CONDITION_ERR must be allowed because"//&
          NEW_LINE('A')//&
          "there are pixels out of the globe disk that can't be represented in latlon "//&
          "coordinates"//NEW_LINE('A')

  if ((fproj_status .ne. FPROJ_NOERR) .and.&
      (fproj_status .ne. FPROJ_TOLERANCE_CONDITION_ERR)) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if  

  !-----------------------------------------------!
  ! Print a 10x10 sample of the image coordinates !
  ! in the destination reference system (Geodetic !
  ! WGS84):                                       !
  !-----------------------------------------------!
  print*, "Image latlong coordinates:"
  print*, "(10x10 pixels sample; image lines:[591, 600]; image columns:[1601, 1610])"//NEW_LINE('A')
  print '("long:"/(10F12.4))', ((long(iter_y, iter_x), iter_x=1601, 1610), iter_y=591, 600)
  print '("lat: "/(10F12.4))', ((lat(iter_y, iter_x) , iter_x=1601, 1610), iter_y=591, 600)
  print*, NEW_LINE('A')

  !-------------------------------------------------!
  ! Define another coordinates reference system     !
  ! (Albers Equal Area) to be used as a destination !
  ! projection:                                     !
  !-------------------------------------------------!
  proj_str = '+proj=aea '//&
             '+ellps=WGS84 '//&
             '+lat_1=52.8333320617676 '//&
             '+lat_2=68.1666641235352 '//&
             '+lon_0=33.5'//&
             '+lat_0=60.5 '//&
             '+x_0=1903970.98145531 '//&
             '+y_0=898179.31322811'

  fproj_status = fproj_init(dst_proj, proj_str)
  if (fproj_status .ne. FPROJ_NOERR) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if

  !---------------------------------------------------!
  ! Use the fproj_transform function to transform the !
  ! source coordinates system (Geostationary)         !
  ! into the destination projection (Albers Equal     !
  ! Area) coordinates:                                !    
  !---------------------------------------------------!
  fproj_status = fproj_transform(src_proj, dst_proj, x2D, y2D)
  if ((fproj_status .ne. FPROJ_NOERR) .and.&
      (fproj_status .ne. FPROJ_TOLERANCE_CONDITION_ERR)) then
     print*, fproj_strerrno(fproj_status)
     stop
  end if  

  !-----------------------------------------------!
  ! Deallocate the projection definition objects: !
  !-----------------------------------------------!
  fproj_status = fproj_free(src_proj)
  if (fproj_status .ne. FPROJ_NOERR) then
      print*, fproj_strerrno(fproj_status)
      stop
  end if

  fproj_status = fproj_free(dst_proj)
  if (fproj_status .ne. FPROJ_NOERR) then
      print*, fproj_strerrno(fproj_status)
      stop
  end if


  !------------------------------------------------------!
  ! Print a 10x10 sample of the image coordinates in the !
  ! destination reference system (Albers Equal Area):    !
  !------------------------------------------------------!
  print*, "Image coordinates in the Albers Equal Area projection:"
  print*, "(10x10 pixels sample; image lines:[591, 600]; image columns:[1601, 1610])"//NEW_LINE('A')
  print '("x (aea):"/(10F12.2))', ((x2D(iter_y, iter_x), iter_x=1601, 1610), iter_y=591, 600)
  print '("y (aea):"/(10F12.2))', ((y2D(iter_y, iter_x), iter_x=1601, 1610), iter_y=591, 600)
  print*, NEW_LINE('A')



end program fproj_example



!> NOTE: The following interfaces can handle point data, 1D array and 2D array.



!!              INTERFACE: fproj_transform
!!
!> fproj_transform(srcdefn, dstdefn, x, y, z, point_offset) result(stat)
!!
!!
!> @param[in] srcdefn Source (input) coordinate system.
!!
!> @param[in] dstdefn Destination (output) coordinate system.     
!!
!> @param[inout] x, y, z The array/element of X, Y and Z coordinate values
!!                        passed as input, and modified in place for output.
!!                        The Z may be optional.
!!
!> @param[in] point_offset The step size from value to value (measured in doubles)
!!                          within the x/y/z arrays - normally 1 for a
!!                          packed array. May be used to operate on xyz interleaved
!!                          point arrays. It is an optional argument.
!!
!> @return stat The return is zero on success, or a PROJ error code.



!!              INTERFACE: fproj_transfer
!!
!> fproj_transfer(srcdefn, dstdefn, x, y, x_t, y_t) result(stat)
!!
!!
!> @param[in] srcdefn Source (input) coordinate system.
!!
!> @param[in] dstdefn Destination (output) coordinate system.     
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the source coordinate system.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the
!!                      destination coordinate system.
!!
!> @return stat The return is zero on success, or a PROJ error code.



!!              INTERFACE: fproj_fwd
!!
!> fproj_fwd(prjdefn, x, y, x_t, y_t) result(stat)
!!
!!
!> @param[in] prjdefn Destination (output)  coordinate system.
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the Geodetic (long,lat)
!!                 coordinate system.
!!                 IMPORTANT: The input long/lat coordinates units
!!                 must be in degrees, not radians.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the
!!                      destination coordinate system.
!!
!> @return stat The return is zero on success, or a PROJ error code.



!!              INTERFACE: fproj_inv
!!
!> fproj_inv(prjdefn, x, y, x_t, y_t) result(stat)
!!
!!
!> @param[in] prjdefn Source (input)   coordinate system.
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the source coordinate system.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the Geodetic
!!                      (long,lat) coordinate system.
!!                      IMPORTANT: The output long/lat coordinates units
!!                      will be in degrees, not radians.
!!
!> @return stat The return is zero on success, or a PROJ error code.




!!              ERROR CODES LIST:
!!
!! (Chekout for the native PROJ errno source code in
!!  pj_strerrno.c file)
!!
!!
!> FPROJ_NOERR                                =   0
!! errors not found
!!
!> FPROJ_MISS_INIT_LIST_ARGS                  =  -1
!! no arguments in initialization list
!!
!> FPROJ_MISS_INIT_FILE_OPTS                  =  -2
!! no options found in 'init' file
!!
!> FPROJ_MISS_COLON_INIT_STR                  =  -3
!! no colon in init= string
!!
!> FPROJ_MISS_PROJ_NAME                       =  -4
!! projection not named
!!
!> FPROJ_UNKNOWN_PROJ_ID                      =  -5
!! unknown projection id
!!
!> FPROJ_INVALID_EFFECT_ECCENTRICITY          =  -6
!! effective eccentricity = 1
!!
!> FPROJ_UNKNOWN_UNIT_CONV_ID                 =  -7
!! unknown unit conversion id
!!
!> FPROJ_INVALID_BOOL_PARAM                   =  -8
!! invalid boolean param argument
!!
!> FPROJ_UNKNOWN_ELLIPTICAL_PARAM             =  -9
!! unknown elliptical parameter name
!!
!> FPROJ_INVALID_FLATTENING                   = -10
!! reciprocal flattening (1/f) = 0
!!
!> FPROJ_INVALID_RADIUS_REF_LAT               = -11
!! |radius reference latitude| > 90
!!
!> FPROJ_INVALID_ECCENTRICITY                 = -12
!! squared eccentricity < 0
!!
!> FPROJ_MISS_EARTH_RADIUS                    = -13
!! major axis or radius = 0 or not given
!!
!> FPROJ_LAT_LON_OUT_OF_BOUNDS                = -14
!! latitude or longitude exceeded limits
!!
!> FPROJ_INVALID_X_Y                          = -15
!! invalid x or y
!!
!> FPROJ_DMS_FORMAT_ERR                       = -16
!! improperly formed DMS value
!!
!> FPROJ_NON_CONVERGENT_INV_MERIDIONAL_DIST   = -17
!! non-convergent inverse meridional dist
!!
!> FPROJ_NON_CONVERGENT_INV_PHI2              = -18
!! non-convergent inverse phi2
!!
!> FPROJ_INVALID_ANGLE                        = -19
!! acos/asin: |arg| >1.+1e-14
!!
!> FPROJ_TOLERANCE_CONDITION_ERR              = -20
!! tolerance condition error
!!
!> FPROJ_INVALID_CONIC_LAT1_LAT2              = -21
!! conic lat_1 = -lat_2
!!
!> FPROJ_LAT1_OUT_OF_BOUNDS                   = -22
!! lat_1 >= 90
!!
!> FPROJ_INVALID_LAT1                         = -23
!! lat_1 = 0
!!
!> FPROJ_LAT_TS_OUT_OF_BOUNDS                 = -24
!! lat_ts >= 90
!!
!> FPROJ_INVALID_CTRL_POINTS_DIST             = -25
!! no distance between control points
!!
!> FPROJ_PROJ_MISS_SELECT_ROTATED             = -26
!! projection not selected to be rotated
!!
!> FPROJ_INVALID_W_M                          = -27
!! W <= 0 or M <= 0
!!
!> FPROJ_LSAT_OUT_OF_RANGE                    = -28
!! lsat not in 1-5 range
!!
!> FPROJ_PATH_OUT_OF_RANGE                    = -29
!! path not in range
!!
!> FPROJ_INVALID_H                            = -30
!! h <= 0
!!
!> FPROJ_INVALID_K                            = -31
!! k <= 0
!!
!> FPROJ_INVALID_LAT0_ALPHA                   = -32
!! lat_0 = 0 or 90 or alpha = 90
!!
!> FPROJ_INVALID_LAT1_LAT2                    = -33
!! lat_1=lat_2 or lat_1=0 or lat_2=90
!!
!> FPROJ_ELLIPTICAL_REQUIRED                  = -34
!! elliptical usage required
!!
!> FPROJ_INVALID_UTM_ZONE                     = -35
!! invalid UTM zone number
!!
!> FPROJ_TCHEBY_ARGS_OUT_OF_RANGE             = -36
!! arg(s) out of range for Tcheby eval
!!
!> FPROJ_PROJ_NOT_FOUND_ROTATED               = -37
!! failed to find projection to be rotated
!!
!> FPROJ_DATUM_SHIFT_LOAD_ERR                 = -38
!! failed to load datum shift file
!!
!> FPROJ_MN_DEFINITION_ERR                    = -39
!! both n & m must be spec'd and > 0
!!
!> FPROJ_N_DEFINITION_ERR                     = -40
!! n <= 0, n > 1 or not specified
!!
!> FPROJ_MISS_LAT1_LAT2                       = -41
!! lat_1 or lat_2 not specified
!!
!> FPROJ_INCONCISTENT_LAT1_LAT2               = -42
!! |lat_1| == |lat_2|
!!
!> FPROJ_LAT0_DEFINED_FROM_MEAN_LAT           = -43
!! lat_0 is pi/2 from mean lat
!!
!> FPROJ_PROJ_DEFINITION_PARSE_ERR            = -44
!! unparseable coordinate system definition
!!
!> FPROJ_MISS_GEOCENTRIC_PARAM                = -45
!! geocentric transformation missing z or ellps
!!
!> FPROJ_UNKNOWN_PRIME_MERIDIAN_CONV_ID       = -46
!! unknown prime meridian conversion id
!!
!> FPROJ_AXIS_ORIENTATION_ERR                 = -47
!! illegal axis orientation combination
!!
!> FPROJ_POINT_NOT_FOUND_DATUM_SHIFT_GRIDS    = -48
!! point not within available datum shift grids
!!
!> FPROJ_INVALID_SWEEP_AXIS                   = -49
!! invalid sweep axis, choose x or y
!!
!> FPROJ_MALFORMED_PIPELINE                   = -50
!! malformed pipeline
!!
!> FPROJ_INVALID_UNIT_CONV_FACTOR             = -51
!! unit conversion factor must be > 0
!!
!> FPROJ_INVALID_SCALE                        = -52
!! invalid scale
!!
!> FPROJ_NON_CONVERGENT_COMPUTATION           = -53
!! non-convergent computation
!!
!> FPROJ_MISS_ARGS                            = -54
!! missing required arguments
!!
!> FPROJ_INVALID_LAT0                         = -55
!! lat_0 = 0
!!
!> FPROJ_ELLIPSOIDAL_USAGE_UNSUPPORTED        = -56
!! ellipsoidal usage unsupported
!!
!> FPROJ_INVALID_INIT_PARAMETER               = -57
!! only one +init allowed for non-pipeline operations
!!
!> FPROJ_INVALID_ARG                          = -58
!! argument not numerical or out of range
!!
!> FPROJ_INCONSISTENT_UNIT_TYPES              = -59
!! inconsistent unit type between input and output
