# Fortran-Proj

The Fortran-Proj library provides fortran binding functions for the PROJ native library (C).
<p>
PROJ is a generic coordinate transformation software, that transforms geospatial coordinates from one coordinate reference system (CRS) to another. This includes cartographic projections as well as geodetic transformations.
<\p>
<p>
Detailed information about the native PROJ library can be found at:
https://proj4.org/index.html
<\p>

**First of all, don't forget to install the native PROJ headers (libproj-dev)...**


* DOWNLOAD, BUILD AND INSTALL (Fortran-Proj):

```
~$ git clone https://gitlab.com/likeno/fortran-proj.git
~$ mkdir fortran-proj/build
~$ cd fortran-proj/build
```
run CMake inside the build directory:
```
~$ cmake ..
```
alternatively you can specify native PROJ path, if not installed in system default paths, and/or specify the install destination path.
```
~$ cmake -DCMAKE_INCLUDE_PATH=/native/proj/path/include/\
         -DCMAKE_LIBRARY_PATH=/native/proj/path/lib/\
         -DCMAKE_INSTALL_PREFIX=/fproj/install/path ..                           
```

```
~$ make
~$ make install
```

**Run the _fproj_example_ program inside the build directory to make sure it has been successfully installed and take a look inside example/fproj_example.f90 to understand how to use Fortran-Proj.**

* COMPILE YOUR CODE:

Compile your fortran program using the <code>-lfproj -lproj</code> flags
to link the PROJ and Fortran-Proj libraries and <code>-c -I</code> to specify
the include path containing the _fproj.mod_ anf _proj.h_ files.
```
~$ <FC> -c -I/fproj/install/path/include -o your_fortran_program.o your_fortran_program.f90
~$ <FC> your_fortran_program.o -lfproj -lproj  -o your_fortran_program
```

(Replace \<FC\> by your fortran compiler)

Special thanks to [Magnus Hangdorn](mailto:magnus.hagdorn@marsupium.org) 
that has given an inspiring contribute for this work!


